Name: cassandra-php-driver
Version: 1.1.0
Release: 1%{?dist}
Summary: The pre-compiled PHP Library Extension for Cassandra as maintained by Datastax
Group: Keek/Driver
License: Apache License, Version 2.0
URL: https://bitbucket.org/keekit/cassandra-php-driver

%define system el7.centos
%define arch x86_64
%define module cassandra

Source0: %{name}-%{version}.%{system}.%{arch}.tar.gz
BuildArch: %{arch}
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot-%(%{__id_u} -n)
Requires: libuv
Requires: gmp
Requires: gmp-devel

%description
The pre-compiled PHP Extension for Cassandra maintained by Datastax
The original source repo by Datastax is found at https://github.com/datastax/php-driver

%prep
%setup -q -n %{name}-%{version}.%{system}.%{arch}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}
cp -R * %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(0644,root,root,0755)
%{_sysconfdir}/php.d/%{module}.ini
%{_libdir}/php/modules/%{module}.so
